
#include <stdio.h>
#include <string.h>

#include "io.h"
#include "delay.h"
#include "cmd.h"

int cmd_void(char *args);
int cmd_test(char *args);
int cmd_devid(char *args);
int cmd_read(char *args);
int cmd_sx0(char *args);
int cmd_sx1(char *args);
int cmd_sx2(char *args);
int cmd_sx3(char *args);
int cmd_sx4(char *args);
int cmd_sx5(char *args);
int cmd_sx6(char *args);
int cmd_sx7(char *args);
int cmd_ret(char *args);

struct cmd_t cmds[] = {
 {"", cmd_void},
 {"test", cmd_test},
 {"devid", cmd_devid},
 {"read", cmd_read},
 {"sx0", cmd_sx0},
 {"sx1", cmd_sx1},
 {"sx2", cmd_sx2},
 {"sx3", cmd_sx3},
 {"sx4", cmd_sx4},
 {"sx5", cmd_sx5},
 {"sx6", cmd_sx6},
 {"sx7", cmd_sx7},
 {"ret", cmd_ret},
 {0,}
};

void system_init();

int main(void){

 system_init();

 io_printf("XX\r\n");

 cmd_loop();

 while(1)
  ;

 return 0;
}

int cmd_void(char *args){
 return 0;
}

int cmd_test(char *args){
 io_printf("Hey!!\r\n");
 return 0;
}
