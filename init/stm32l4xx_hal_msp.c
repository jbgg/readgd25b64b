#include "stm32l4xx_hal.h"

void HAL_MspInit(){
 __HAL_RCC_SYSCFG_CLK_ENABLE();
 __HAL_RCC_PWR_CLK_ENABLE();
}

void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi){
 GPIO_InitTypeDef  GPIO_InitStruct;
 if(hspi->Instance == SPI1){
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_SPI1_CLK_ENABLE();

  /* SCK */
  GPIO_InitStruct.Pin       = GPIO_PIN_5;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* MISO */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* MOSI */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
 }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi){
 if(hspi->Instance == SPI1){
  __HAL_RCC_SPI1_FORCE_RESET();
  __HAL_RCC_SPI1_RELEASE_RESET();

  HAL_GPIO_DeInit(GPIOA, GPIO_PIN_5);
  HAL_GPIO_DeInit(GPIOA, GPIO_PIN_6);
  HAL_GPIO_DeInit(GPIOA, GPIO_PIN_7);
 }
}

void HAL_UART_MspInit(UART_HandleTypeDef *huart){
 GPIO_InitTypeDef  GPIO_InitStruct;

 __HAL_RCC_GPIOA_CLK_ENABLE();
 __HAL_RCC_USART2_CLK_ENABLE();

 GPIO_InitStruct.Pin       = GPIO_PIN_2;
 GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
 GPIO_InitStruct.Pull      = GPIO_PULLUP;
 GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
 GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
 HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

 GPIO_InitStruct.Pin       = GPIO_PIN_15;
 GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
 GPIO_InitStruct.Pull      = GPIO_PULLUP;
 GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
 GPIO_InitStruct.Alternate = GPIO_AF3_USART2;
 HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void HAL_UART_MspDeInit(UART_HandleTypeDef *huart){
 __HAL_RCC_USART2_FORCE_RESET();
 __HAL_RCC_USART2_RELEASE_RESET();

 HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2);
 HAL_GPIO_DeInit(GPIOA, GPIO_PIN_3);
}
