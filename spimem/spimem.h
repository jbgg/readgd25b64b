#ifndef __SPIMEM_H__
#define __SPIMEM_H__

#include <stdint.h>

int spimem_devid(uint8_t *d);
int spimem_read(uint8_t *d, uint32_t addr, uint16_t s);

#endif /* __SPIMEM_H__ */
