
#include <stddef.h>

#include "spi.h"
#include "spimem.h"
#include "xmodem.h"
#include "io.h"

int ret = 1;

int spimem_devid(uint8_t *o){
 if(o == NULL){
  return 1;
 }
 spi_cs(0);
 uint8_t d[4];
 d[0] = 0x90;
 d[1] = 0; d[2] = 0; d[3] = 0;
 if(spi_w(d, 4)){
  spi_cs(1);
  return 1;
 }
 if(spi_r(o, 2)){
  spi_cs(1);
  return 1;
 }
 spi_cs(1);
 return 0;
}

int spimem_read(uint8_t *o, uint32_t addr, uint16_t s){
 int i;
 if(o == NULL){
  return 1;
 }
 if((addr & 0xffffff) != addr){
  return 1;
 }
 spi_cs(0);
 uint8_t d[4];
 d[0] = 0x03;
 d[1] = (addr >> 16) & 0xff;
 d[2] = (addr >> 8) & 0xff;
 d[3] = addr & 0xff;
 if(spi_w(d, 4)){
  spi_cs(1);
  return 1;
 }
 for(i=0;i<s;i++){
  if(spi_r(&o[i], 1)){
   spi_cs(1);
   return 1;
  }
 }
 spi_cs(1);
 return 0;
}

int cmd_devid(char *args){
 uint8_t d[2];
 if(spimem_devid(d)){
  io_printf("error reading devid\r\n");
  return 1;
 }
 io_printf("manufacter ID: %02x\r\n", d[0]);
 io_printf("device ID: %02x\r\n", d[1]);
 return 0;
}

int cmd_read(char *args){
 /* TODO: read addr as argument */
 int i;
 int j;
 uint8_t d[256];
 uint32_t addr;

 addr = 0x000000;
 if(spimem_read(d, addr, 256)){
  io_printf("error reading data\r\n");
  return 1;
 }
 for(i=0;i<256;){
  io_printf("%06x:", addr);
  for(j=0;j<16;j++){
   io_printf(" %02x", d[i]);
   if(j==7){
    io_printf(" ");
   }
   i++;
  }
  io_printf("\r\n");
  addr += 16;
 }
 return 0;
}

int sx(uint32_t addr){

 int i;
 char d[1024];

 ret = 1;
 if(xmodem_init()){
  return 1;
 }
 for(i=0;i<1024;i++){
  if(spimem_read(d, addr, 1024)){
   return 1;
  }
  if(xmodem_send(d, 1024)){
   return 1;
  }
  addr += 1024;
 }
 if(xmodem_end()){
  return 1;
 }else{
  ret = 0;
 }

 return 0;
}

int cmd_sx0(char *args){
 if(sx(0x000000)){
  return 1;
 }
 return 0;
}
int cmd_sx1(char *args){
 if(sx(0x100000)){
  return 1;
 }
 return 0;
}
int cmd_sx2(char *args){
 if(sx(0x200000)){
  return 1;
 }
 return 0;
}
int cmd_sx3(char *args){
 if(sx(0x300000)){
  return 1;
 }
 return 0;
}
int cmd_sx4(char *args){
 if(sx(0x400000)){
  return 1;
 }
 return 0;
}
int cmd_sx5(char *args){
 if(sx(0x500000)){
  return 1;
 }
 return 0;
}
int cmd_sx6(char *args){
 if(sx(0x600000)){
  return 1;
 }
 return 0;
}
int cmd_sx7(char *args){
 if(sx(0x700000)){
  return 1;
 }
 return 0;
}

int cmd_ret(char *args){
 io_printf("%d\r\n", ret);
 return 0;
}
