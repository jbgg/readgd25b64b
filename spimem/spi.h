
#ifndef __SPI_H__
#define __SPI_H__

#include <stdint.h>

void spi_cs(uint8_t v);
int spi_w(uint8_t *d, uint16_t s);
int spi_r(uint8_t *d, uint16_t s);

#endif /* __SPI_H__ */
