
#include "spi.h"
#include "stm32l4xx_hal.h"

extern SPI_HandleTypeDef SpiHandle;

void spi_cs(uint8_t v){
 HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4,
   v ? GPIO_PIN_SET : GPIO_PIN_RESET);
}

int spi_w(uint8_t *d, uint16_t s){
 if(HAL_SPI_Transmit(&SpiHandle, d, s, 1000) != HAL_OK){
  return 1;
 }
 return 0;
}

int spi_r(uint8_t *d, uint16_t s){
 if(HAL_SPI_Receive(&SpiHandle, d, s, 1000) != HAL_OK){
  return 1;
 }
 return 0;
}
